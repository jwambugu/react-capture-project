import React from "react";

// Global styles
import GlobalStyles from "./components/GlobalStyles";

// Import Pages
import AboutUs from "./pages/AboutUs";
import ContactUs from "./pages/ContactUs";
import OurWork from "./pages/OurWork";
import Navbar from "./components/Navbar";
import MovieDetail from "./pages/MovieDetail";

// Router
import { Switch, Route, useLocation } from "react-router-dom";

// Animation
import { AnimatePresence } from "framer-motion";

function App() {
    const location = useLocation();

    return (
        <div className="App">
            <header className="App-header">
                <GlobalStyles />
                <Navbar />
                <AnimatePresence exitBeforeEnter>
                    <Switch location={location} key={location.pathname}>
                        <Route exact path="/" component={AboutUs}></Route>
                        <Route
                            exact
                            path="/our-work"
                            component={OurWork}
                        ></Route>
                        <Route
                            exact
                            path="/our-work/:id"
                            component={MovieDetail}
                        ></Route>
                        <Route
                            exact
                            path="/contact-us"
                            component={ContactUs}
                        ></Route>
                    </Switch>
                </AnimatePresence>
            </header>
        </div>
    );
}

export default App;
