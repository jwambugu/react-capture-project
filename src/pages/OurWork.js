import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";

// Import images
import athlete from "../img/athlete-small.png";
import theRacer from "../img/theracer-small.png";
import goodTimes from "../img/goodtimes-small.png";

// Import animations
import { motion } from "framer-motion";
import {
    pageAnimation,
    fade,
    photoAnimation,
    lineAnimation,
    sliderAnimation,
    sliderAnimationContainer,
} from "../animation";

import { useScroll } from "../components/useScroll";
import ScrollTop from "../components/ScrollTop";

const OurWork = () => {
    const [element, controls] = useScroll();
    const [elementTwo, controlsTwo] = useScroll();

    return (
        <Work
            variants={pageAnimation}
            initial="hidden"
            animate="show"
            exit="exit"
            style={{ background: "#ffffff" }}
        >
            <ScrollTop />
            <motion.div variants={sliderAnimationContainer}>
                <FrameOne variants={sliderAnimation}></FrameOne>
                <FrameTwo variants={sliderAnimation}></FrameTwo>
                <FrameThree variants={sliderAnimation}></FrameThree>
                <FrameFour variants={sliderAnimation}></FrameFour>
            </motion.div>
            <Movie>
                <motion.h2 variants={fade}>The Athlete</motion.h2>
                <motion.div
                    variants={lineAnimation}
                    className="line"
                ></motion.div>
                <Link to="/our-work/the-athlete">
                    <Hide>
                        <motion.img
                            variants={photoAnimation}
                            src={athlete}
                            alt="athlete"
                        />
                    </Hide>
                </Link>
            </Movie>
            <Movie
                ref={element}
                variants={fade}
                animate={controls}
                initial="hidden"
            >
                <h2>The Racer</h2>
                <motion.div
                    variants={lineAnimation}
                    className="line"
                ></motion.div>
                <Link to="/our-work/the-racer">
                    <Hide>
                        <img src={theRacer} alt="the racer" />
                    </Hide>
                </Link>
            </Movie>
            <Movie
                ref={elementTwo}
                variants={fade}
                animate={controlsTwo}
                initial="hidden"
            >
                <h2>The Good Time</h2>
                <motion.div
                    variants={lineAnimation}
                    className="line"
                ></motion.div>
                <Link to="/our-work/good-times">
                    <img src={goodTimes} alt="the good time" />
                </Link>
            </Movie>
        </Work>
    );
};

const Work = styled(motion.div)`
    min-height: 100vh;
    overflow: hidden;
    padding: 5rem 10rem;
    h2 {
        padding: 1rem 0rem;
    }

    @media (max-width: 1300px) {
        padding: 2rem 2rem;
    }
`;

const Movie = styled(motion.div)`
    padding-bottom: 10rem;
    .line {
        height: 0.5rem;
        background: #cccccc;
        margin-bottom: 3rem;
        background: #23d997;
    }
    img {
        width: 100%;
        height: 70vh;
        object-fit: cover;
    }
    h2 {
    }
`;

const Hide = styled.div`
    overflow: hidden;
`;

// Frame Animations
const FrameOne = styled(motion.div)`
    position: fixed;
    left: 0;
    top: 10%;
    width: 100%;
    height: 100vh;
    background: #fffebf;
    z-index: 2;
`;

const FrameTwo = styled(FrameOne)`
    background: #ff8efb;
`;

const FrameThree = styled(FrameOne)`
    background: #8ed2ff;
`;

const FrameFour = styled(FrameOne)`
    background: #8effa0;
`;

export default OurWork;
